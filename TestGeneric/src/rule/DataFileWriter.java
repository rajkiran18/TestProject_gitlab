package rule;

import java.util.List;
import java.util.Properties;

public interface DataFileWriter {
	
   
    
    void writeToFile(String fileName, DataRecordImpl impl, DataRecordType dataRecordType);
}
