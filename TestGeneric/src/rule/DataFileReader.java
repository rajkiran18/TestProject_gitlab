package rule;

import java.util.List;
import java.util.Properties;

public interface DataFileReader {
    
	DataRecordImpl readFromFile(String fileName, Properties prop, FileTypes fileTypes);
}
