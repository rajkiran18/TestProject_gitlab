package rule;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.*;

public class DataRecordImpl <T> implements Record{
	
	private List <T> records;
	DataRecordType dataRecordType;
	
	/*public DataRecordImpl(){
		
	}
	*/
	
	public DataRecordImpl(List <T> records,DataRecordType dataRecordType){
		
		this.records=records;
		this.dataRecordType=dataRecordType;
	} 
	
	
	
	public int getRecordCount()
	{
		int count=0;
		if(records != null)
		{
			count=this.records.size();
		}
		return count;
	}
	
	
	
	public DataRecordType getDataRecordType()
	{
		return this.dataRecordType;
	}
	
	
	
	public List <T> getAllRecords()
	{
		return this.records;
	}
	
	
	
	
	public T getRecord(int recordCount)
	{
		return this.records.get(recordCount);
	}
	
	
	
	//---------------------------------------------------------------------
	

}
