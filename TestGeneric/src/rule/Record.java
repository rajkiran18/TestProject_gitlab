package rule;

import java.util.List;

public interface Record {
	
	public int getRecordCount();
	
	public DataRecordType getDataRecordType();
	
	public<T> List <T> getAllRecords();
	
	public<T> T getRecord(int recordCount);
	
}
