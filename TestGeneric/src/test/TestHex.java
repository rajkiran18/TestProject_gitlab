package test;

public class TestHex {

	
	public static void main(String[] args) {
		
		
		/*String strBin =  convertTagBinary(netcTagGenDTO.getTgHeader(),8)+convertTagBinary(netcTagGenDTO.getTgFilter(),3)
		+convertTagBinary(netcTagGenDTO.getTgPartition(),3)
		+convertTagBinary(netcTagGenDTO.getCountryCode()+netcTagGenDTO.getIhmcl(),10+14)
		+convertTagBinary(netcTagGenDTO.getCchId(),5)+convertTagBinary(""+issuerId,20)
		+convertTagBinary(""+issuerKeyIndex,8)+convertTagBinary(""+vehIdSeq,20)
		+convertTagBinary(netcTagGenDTO.getRfu(),5);


		LoggerUtil.doLog(LTI, this.getClass().getSimpleName(), "execute", batchId, batchRunId, "completeBinary: ["+strBin+"] len:"+strBin.length());
		String hexEpcId=convertTagBinToHex(strBin).toUpperCase();*/
		
		String strBin="11111111";
		String hexEpcId=convertTagBinToHex(strBin);
		
		/*System.out.println("hexEpcId:"+hexEpcId);
		System.out.println(""+convertTagBinToHex(hexEpcId));*/
		
		int decimal = Integer.parseInt(strBin.substring(0, 8),2);
		System.out.println("decimal:"+decimal);
		String hexStr =Integer.toString(decimal,16);
		System.out.println("hexStr:"+hexStr);
		
		byte extra_dop = (byte)Integer.parseInt("11111111", 2);
		System.out.println("extra_dop:"+extra_dop);
		
		
		int parseInt = Integer.parseInt("11111111", 2);
		char c = (char)parseInt;
		System.out.println(c);
		
		
		
		
		int x=00000001/10001;
		System.out.println(x);
		int xxxxx =Integer.parseInt(""+x,2);
		System.out.println(xxxxx);
		String hexStrX =Integer.toString(xxxxx,16);
		System.out.println(hexStrX);
		
	}
	

	String convertTagBinary(String field,int len)
	{
		String strBin="";
		strBin=Long.toBinaryString(Long.parseLong(field));

		int binLen=strBin.length();
		if(binLen< len)
		{
			int paddedLen=len-binLen;
			String paddedStr="";
			for(int i=1;i<=paddedLen;i++)
			{
				paddedStr=paddedStr+"0";
			}
			strBin=paddedStr+strBin;	

		}	

		return strBin;
	}


	static String convertTagBinToHex(String strBin)
	{

		String strSplit="";
		String hexStr="";
		int totalLen=strBin.length();
		for(int i=0;i<totalLen/4;i++)
		{
			strSplit=strSplit+strBin.substring(0, 4)+" ";
			int decimal = Integer.parseInt(strBin.substring(0, 4),2);
			hexStr =hexStr+ Integer.toString(decimal,16);		

			if((i*4+4)<totalLen)
			{strBin=strBin.substring(4);}

		}

		return hexStr;
	}




	public static String leftPadding(String str, int length, char ch)
	{
		if (str != null)
		{
			String f1 = str;
			String f2 = "";
			int lyy = f1.length();
			if (lyy < length)
			{
				int padding = length - lyy;
				for (int i = 1; i <= padding; i++)
				{
					f2 = f2 + ch;
				}
				f1 = f2 + f1;
			}
			str = f1;
		}
		return str;
	}


	public static String rightPadding(String str, int length, char ch)
	{
		if (str != null)
		{
			String f1 = str;
			String f2 = "";
			int lyy = f1.length();
			if (lyy < length)
			{
				int padding = length - lyy;
				for (int i = 1; i <= padding; i++)
				{
					f2 = f2 + ch;
				}
				f1 = f1 + f2;
			}
			str = f1;
		}
		return str;
	}


}
